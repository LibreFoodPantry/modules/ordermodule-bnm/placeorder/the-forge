const express = require('express');
const app = express();

const orders = [
	{id: 200001, type: 'goldenBearOrder', timestamp: 3102020, userID: 'aa356640'},
	{id: 200002, type: 'customOrder', timestamp: 3092020, userID: 'nl256874'},
	{id: 200003, type: 'goldenBearOrder', timestamp: 3022020, userID: 'eg458996'} 
]; 
// Have to decide how we will be storing custom order items. 
// 0 prior to date changes overall value
// String treated different in api URL

app.get('/', (req,res) => {
	res.send('Place Order');
});

app.get('/api/orders', (req,res) => {
	res.send([orders]);
});

app.get('/api/orders/time/:timestamp', (req, res) => {
	const orderByTime = orders.find(c => c.timestamp === parseInt(req.params.timestamp));
	if(!(orderByTime)) res.status(404).send('Order with given timestamp not found');
	res.send(orderByTime);
});

app.get('/api/orders/byID/:id', (req,res) => {
	const order = orders.find(c => c.id === parseInt(req.params.id));
	if(!order) res.status(404).send('Order with id was not found'); //404 (not found)
	res.send(order);
});

app.get('/api/orders/byUserID/:userID', (req,res) => {
	const orderByID = orders.find(c => c.userID === (req.params.userID));
	if(!orderByID) res.status(404).send('No order associated with given userID');
	res.send(orderByID);
});


// PORT - Have to find port used for BNM
const port = process.env.PORT || 3000; //Using 3000 for dev
app.listen(port,() => console.log(`Listening on port ${port}...`));

